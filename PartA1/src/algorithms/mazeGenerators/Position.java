package algorithms.mazeGenerators;

public class Position {

    private int row;
    private int col;

    public Position() {}

    public Position(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRowIndex() {
        return row;
    }


    public int getColumnIndex() {
        return col;
    }


    public void printPosition(){
        System.out.println("{"+this.getRowIndex()+","+this.getColumnIndex()+"}");
    }

    public String toString(){
        return("{"+this.getRowIndex()+","+this.getColumnIndex()+"}");
    }

    public Position fromString(String s, Position p){
        String[] sarr = s.split(",");
        p.row = Integer.parseInt(sarr[0]);
        p.col = Integer.parseInt(sarr[1]);
        return p;
    }

    public boolean isEqual(Position p1){
        return (this.getColumnIndex()== p1.getColumnIndex()) && (this.getRowIndex()==p1.getRowIndex());
    }

}
