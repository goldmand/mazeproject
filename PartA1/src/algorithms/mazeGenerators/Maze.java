package algorithms.mazeGenerators;
import algorithms.mazeGenerators.search.AState;
import algorithms.mazeGenerators.search.MazeState;
import javafx.geometry.Pos;

import java.io.*;
import java.util.*;

public class Maze {
    private int[][] body;
    private  int[][] visited;
    private int numofcols;
    private int numofrows;
    private Position startPosition;
    private Position goalPosition;

    public Maze(int rows, int cols,Position p1, Position p2){
        body=new int[rows][cols];
        visited=new int[rows][cols];
        for(int i=0; i<rows; i++)
            for(int j=0; j<cols; j++)
                visited[i][j]=0;

        this.numofcols=cols;
        this.numofrows=rows;
        this.startPosition=p1;
        this.goalPosition=p2;
    }


    public int getNumofcols() {
        return numofcols;
    }

    public int getNumofrows() {
        return numofrows;
    }

    public void setCellValue(int row, int col, int val) {
        this.body[row][col] = val;
    }

    public int getCellValue(int row, int col) {
        return this.body[row][col];
    }

    public void setCellVisited(int row, int col, int val) {
        this.visited[row][col] = val;
    }

    public int getCellVisited(int row, int col) {
        return this.visited[row][col];
    }


    public Position getStartPosition(){
        return this.startPosition;
    }

    public Position getGoalPosition(){
        return this.goalPosition;
    }

    public void setStartPosition(Position startPosition) {
        this.startPosition = startPosition;
    }

    public void setGoalPosition(Position goalPosition) {
        this.goalPosition = goalPosition;
    }


    public void print(){

        for (int i = 0; i< this.numofrows; i++ ) {
            System.out.print("[");
            for (int j = 0; j < this.numofcols; j++) {

                if (this.startPosition != null && this.startPosition.getRowIndex() == i && this.startPosition.getColumnIndex() == j) {
                    System.out.print("S");
                }
                else if (this.goalPosition != null && this.goalPosition.getRowIndex() == i &&this.goalPosition.getColumnIndex() == j) {
                    System.out.print("E");
                }
                else{
                    System.out.print(this.body[i][j]);
                }
                if (j < this.numofcols-1) {
                    System.out.print(",");
                }
            }
            System.out.print("]");
            System.out.println("");
        }

    }
    public void printwithsol2(ArrayList<AState> sol) {
        for(int i = 0; i < this.body.length; ++i) {
            for(int j = 0; j < this.body[i].length; ++j) {
                AState s = new MazeState(new Position(i,j));
                if (i == this.startPosition.getRowIndex() && j == this.startPosition.getColumnIndex()) {
                    System.out.print(" \u001b[32m ");
                } else if (i == this.goalPosition.getRowIndex() && j == this.goalPosition.getColumnIndex()) {
                    System.out.print(" \u001b[31m ");
                } else if (this.body[i][j] == 1) {
                    System.out.print(" \u001b[45m ");
                } else if (sol.contains(s)) {
                    System.out.print(" \u001b[44m ");
                } else {
                    System.out.print(" \u001b[107m ");
                }
            }

            System.out.println(" \u001b[107m");
        }
    }

    public void printwithsol(ArrayList<AState> sol){

        for (int i = 0; i< this.numofrows; i++ ) {
            System.out.print("[");
            for (int j = 0; j < this.numofcols; j++) {
                AState s = new MazeState(new Position(i,j));
                if (this.startPosition != null && this.startPosition.getRowIndex() == i && this.startPosition.getColumnIndex() == j) {
                    System.out.print("\033[1;32m"+ "S" + "\033[0m");
                }
                else if (this.goalPosition != null && this.goalPosition.getRowIndex() == i &&this.goalPosition.getColumnIndex() == j) {
                    System.out.print("\033[1;31m"+ "E" + "\033[0m");
                }

                else if(sol.contains(s)){
                    System.out.print("\033[1;30m"+ "0" + "\033[0m");
                }

                else{
                    System.out.print(this.body[i][j]);
                }
                if (j < this.numofcols-1) {
                    System.out.print(",");
                }
            }
            System.out.print("]");
            System.out.println("");
        }

    }

}



