package algorithms.mazeGenerators;

import java.io.*;
import java.util.*;

public class MyMazeGenerator extends AMazeGenerator {
    //implement generate method with dfs algorithm

    @Override
    public Maze generate(int rows, int cols){

        Random random = new Random();

        Position startP = new Position(random.nextInt(rows), random.nextInt(cols));
        Position endP = new Position(random.nextInt(rows), random.nextInt(cols));

        Maze m = new Maze(rows, cols, startP, endP);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                m.setCellValue(i, j, 1);
            }
        }
         LinkedList<int[]> frontiers = new LinkedList<>();
        int x = random.nextInt(rows);
        int y = random.nextInt(cols);
        frontiers.add(new int[]{x,y,x,y});

        while ( !frontiers.isEmpty() ){
            final int[] f = frontiers.remove( random.nextInt( frontiers.size() ) );
            x = f[2];
            y = f[3];
            if (m.getCellValue(x,y)==1 )
            {
                m.setCellValue(f[0],f[1],0);
                m.setCellValue(x,y,0);

                if ( x >= 2 && m.getCellValue(x-2,y)==1 )
                    frontiers.add( new int[]{x-1,y,x-2,y} );
                if ( y >= 2 && m.getCellValue(x,y-2)==1 )
                    frontiers.add( new int[]{x,y-1,x,y-2} );
                if ( x < rows-2 && m.getCellValue(x+2,y)==1)
                    frontiers.add( new int[]{x+1,y,x+2,y} );
                if ( y < cols-2 && m.getCellValue(x,y+2)==1 )
                    frontiers.add( new int[]{x,y+1,x,y+2} );
            }
        }

        boolean flag = true;
        while (flag) {
            if (startP.isEqual(endP)) { //if we got the same point as both beginning and end
                endP = new Position(random.nextInt(rows), random.nextInt(cols));
            } else if (!checkonframe(startP, rows, cols)) {//making sure start point is on frame
                startP = new Position(random.nextInt(rows), random.nextInt(cols));
            } else if (!checkonframe(endP, rows, cols)) {//making sure end point is on frame
                endP = new Position(random.nextInt(rows), random.nextInt(cols));
            } else if (onsameline(startP, endP)) { //making sure that both points are not on the same line
                endP = new Position(random.nextInt(rows), random.nextInt(cols));
            } else if (getAvailableMoves(startP,m).size()<1) {
                startP = new Position(random.nextInt(rows), random.nextInt(cols));
            } else if (getAvailableMoves(endP,m).size()<1){
                    endP = new Position(random.nextInt(rows), random.nextInt(cols));
            } else {
                flag = false;
            }
        }
        m.setCellValue(startP.getRowIndex(),startP.getColumnIndex(),0);
        m.setCellValue(endP.getRowIndex(),endP.getColumnIndex(),0);
        m.setStartPosition(startP);
        m.setGoalPosition(endP);

     return m;
    }

    public boolean checkonframe(Position p1, int numofrows, int numofcols) {
        return p1.getRowIndex() == 0 || p1.getRowIndex() == numofrows - 1 || p1.getColumnIndex() == 0 || p1.getColumnIndex() == numofcols - 1;
    }

    public boolean onsameline(Position p1, Position p2) {
        return p1.getColumnIndex() == p2.getColumnIndex() || p1.getRowIndex() == p2.getRowIndex();
    }

    public Vector<Position> getAvailableMoves(Position p1, Maze m){

        Vector<Position> p = new Vector<Position>();
        int row = p1.getRowIndex();
        int col = p1.getColumnIndex();

        //0-left , 1-up, 2-right , 3-down
        // left neighbor - 0
        if (row < m.getNumofrows() && col - 1 >= 0 && m.getCellValue(row,col - 1) == 0) {
            Position p2 = new Position(row, col - 1);
            p.add(p2);
        }
        //upper neighbour - 1
        if (col < m.getNumofcols() && row - 1 >= 0  && m.getCellValue(row-1,col ) == 0) {
            Position p2 = new Position(row - 1, col);
            p.add(p2);
        }
        //right neighbor - 2
        if (row < m.getNumofrows() && col + 1 < m.getNumofcols() && m.getCellValue(row,col + 1) == 0) {
            Position p2 = new Position(row, col + 1);
            p.add(p2);
        }
        //lower neighbor - 3
        if (col < m.getNumofcols() && row + 1 < m.getNumofrows() && m.getCellValue(row+1,col ) == 0) {
            Position p2 = new Position(row + 1, col);
            p.add(p2);
        }
        return p;
    }

}