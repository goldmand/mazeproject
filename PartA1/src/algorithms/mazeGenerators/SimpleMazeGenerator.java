package algorithms.mazeGenerators;
import java.util.Random;

public class SimpleMazeGenerator extends AMazeGenerator {

    public boolean checkonframe(Position p1, int numofrows, int numofcols) {
        return p1.getRowIndex() == 0 || p1.getRowIndex() == numofrows - 1 || p1.getColumnIndex() == 0 || p1.getColumnIndex() == numofcols - 1;

    }

    public boolean onsameline(Position p1, Position p2) {
        return p1.getColumnIndex() == p2.getColumnIndex() || p1.getRowIndex() == p2.getRowIndex();
    }

    @Override
    public Maze generate(int rows, int cols) {
        Random random = new Random();
        Position startP = new Position(random.nextInt(rows), random.nextInt(cols));
        Position endP = new Position(random.nextInt(rows), random.nextInt(cols));
        boolean flag = true;
        while (flag) {
            if (startP.isEqual(endP)) { //if we got the same point as both beginning and end
                endP = new Position(random.nextInt(rows), random.nextInt(cols));
            } else if (!checkonframe(startP, rows, cols)) {//making sure start point is on frame
                startP = new Position(random.nextInt(rows), random.nextInt(cols));
            } else if (!checkonframe(endP, rows, cols)) {//making sure end point is on frame
                endP = new Position(random.nextInt(rows), random.nextInt(cols));
            } else if (onsameline(startP, endP)) { //making sure that both points are not on the same line
                endP = new Position(random.nextInt(rows), random.nextInt(cols));
            } else {
                flag = false;
            }
        }
        Maze newMaze = new Maze(rows, cols, startP, endP);
        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < cols; x++) {
                int random1 = random.nextInt(2);
                newMaze.setCellValue(x, y, random1);
            }
        }


        return newMaze;

    }
}
