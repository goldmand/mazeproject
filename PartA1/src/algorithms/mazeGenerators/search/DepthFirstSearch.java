package algorithms.mazeGenerators.search;

import java.util.*;

public class DepthFirstSearch extends ASearchingAlgorithm {

    //private int nodesVisited;

    public DepthFirstSearch() {
        NumberOfVisitedNodes = 0;
    }

    public String getName() {
        return "Depth First Search";
    }

    public String getNumberOfNodesEvaluated() {
        return Integer.toString(NumberOfVisitedNodes);
    }

    public Solution solve(ISearchable s) {
        Solution sol = new Solution();
        //check:
        /*SearchableMaze sm = (SearchableMaze) s;
        boolean[][] visited = new boolean[sm.getNumofRows()][sm.getNumofCols()]; //visited matrix
        //initilize
        for (int i = 0; i < sm.getNumofRows(); i++) {
            for (int j = 0; j < sm.getNumofCols(); j++) {
                visited[i][j] = false;
            }
        }*/

        AState startS = s.getStartState();
        AState endS = s.getGoalState();

        sol.insertToSolution(startS);

        ArrayList<AState> neighbours;

        // Create a stack for DFS
        Stack<AState> stack = new Stack<AState>();

        // Push the current source node.
        stack.push(startS);
        AState curr = startS;
        //curr.setCameFrom(endS);
        //check
        /*MazeState m1 = (MazeState) curr;
        MazeState curr1; */

        boolean found = false;
        while (!stack.isEmpty() && !found) {
            // Pop a vertex from stack and print it
            curr = stack.peek();
            //curr1 = (MazeState) curr;

            // Stack may contain same vertex twice. So
            // we need to print the popped item only
            // if it is not visited.
          /*  if (curr.getCameFrom()) == null) {
                    visited[curr1.getMstate().getRowIndex()][curr1.getMstate().getColumnIndex()] = true;
                NumberOfVisitedNodes++;
            }*/
            if (curr.equals(endS)) {// if found the end position, were done and curr holds the goal state
                found = true;
                sol = getSolfromCurr(curr);
                System.out.println("success");
                break;
            }
            // Get all adjacent vertices of the popped vertex s
            // If a adjacent has not been visited, then push it
            // to the stack.
            neighbours = s.getAllPossibleStates(curr);

            int c = 0;
            int i = 0;
            while (i < neighbours.size() && !found) {
               // MazeState currNeighouber = (MazeState) neighbours.get(i);
               // if (visited[currNeighouber.getMstate().getRowIndex()][currNeighouber.getMstate().getColumnIndex()] == false) {
                   // visited[currNeighouber.getMstate().getRowIndex()][currNeighouber.getMstate().getColumnIndex()] = true;
                if (neighbours.get(i).getCameFrom() == null){
                    NumberOfVisitedNodes++;
                    neighbours.get(i).setCameFrom(curr);
                    c++;
                    stack.push(neighbours.get(i));
                    break;
                }
                i++;
            }
            if (c == 0)
                stack.pop();

        }
        return sol;
    }

    public Solution getSolfromCurr(AState curr) {
        Solution sol = new Solution();
        AState keep = curr;
        while (curr != null) {
            sol.insertToSolution(curr);
            curr = curr.getCameFrom();
            if (curr.equals(keep))
                break;
        }
        return sol;
    }
}
