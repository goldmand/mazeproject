package algorithms.mazeGenerators.search;
import com.sun.javaws.IconUtil;

import java.util.*;

public class BreadthFirstSearch extends ASearchingAlgorithm{

    //private int nodesVisited;
    PriorityQueue<AState> q;

    public BreadthFirstSearch() {
        this.q = new PriorityQueue<AState>(new Comparator<AState>() {
            @Override
            public int compare(AState s1, AState s2){
                if (s1.equals(s2))
                    return 0;
                return 1;

            }
        });
        NumberOfVisitedNodes = 0;
    }

    public String getName(){return "Breadth First Search";}

    public String getNumberOfNodesEvaluated(){return Integer.toString(NumberOfVisitedNodes);}

    public Solution solve(ISearchable s) {

        Solution sol = new Solution();
        
        //SearchableMaze sm=(SearchableMaze)s;
        //boolean[][] visited=new boolean[sm.getNumofRows()][sm.getNumofCols()]; //visited matrix

        //initilize
//        for(int i=0; i<sm.getNumofRows(); i++){
//            for(int j=0; j<sm.getNumofCols(); j++){
//                visited[i][j]=false;
//            }
//        }

        AState startS=s.getStartState();
        AState endS=s.getGoalState();
        
        ArrayList<AState> neighbours;

        q.add(startS);
        AState curr=startS;
        //MazeState m1=(MazeState)curr;
        //MazeState curr1;
        curr.setCameFrom(endS);
        //casting is for the row & col index of the state

        boolean found = false;
        while (!q.isEmpty() && !found) {
            curr=q.remove();

            if(curr.equals(endS)) {// if found the end position, were done and curr holds the goal state
                found = true;
                sol = getSolfromCurr(curr);
                System.out.println("success");
                break;
            }
            else {
                neighbours = s.getAllPossibleStates(curr);
                    int i = 0;
                    while( i < neighbours.size() && !found){ //go through all neighbours of current node
                        //if node unvisited,casting
                        // MazeState currNeighouber = (MazeState) neighbours.get(i);
                        // if (visited[currNeighouber.getMstate().getRowIndex()][currNeighouber.getMstate().getColumnIndex()] == false) {
                        // visited[currNeighouber.getMstate().getRowIndex()][currNeighouber.getMstate().getColumnIndex()] = true;
                        if (neighbours.get(i).getCameFrom() == null){
                            NumberOfVisitedNodes++;
                            neighbours.get(i).setCameFrom(curr);
                            q.add(neighbours.get(i));
                        }
                        i++;
                    }

            }
        }
        return sol;
    }

    public Solution getSolfromCurr(AState curr) {
        Solution sol = new Solution();
        AState keep = curr;
        while (curr != null) {
            sol.insertToSolution(curr);
            curr = curr.getCameFrom();
            if (curr.equals(keep))
                break;
        }
        return sol;
    }
}


