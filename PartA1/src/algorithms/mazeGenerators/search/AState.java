package algorithms.mazeGenerators.search;
import algorithms.mazeGenerators.Position;

import java.io.*;
import java.util.*;

public abstract class AState {
    private String state;//should be string or 2 indexs(x,y)
    private double cost;
    private AState cameFrom;

    public AState() {}

    public AState(String p){this.state=p;}

    public double getCost() {
        return cost;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setCameFrom(AState cameFrom) {
        this.cameFrom = cameFrom;
    }

    public String getState() {return state;}

    public AState getCameFrom() {return cameFrom;}

    public void printState(){}

    public int hashCode() {
        return state != null ? state.hashCode() : 0;
    }
}
