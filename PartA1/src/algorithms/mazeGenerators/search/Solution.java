package algorithms.mazeGenerators.search;


import java.util.ArrayList;
import java.util.Stack;

public class Solution {

    private ArrayList<AState> solution;

    public Solution() {
        this.solution = new ArrayList<AState>();
    }

    public ArrayList<AState> getSolutionPath(){
        ArrayList<AState> sol = new ArrayList<>();
        for(int i =solution.size()-1; i>=0;i--)
            sol.add(solution.get(i));
        return sol;
    }

    public void insertToSolution(AState a){solution.add(a);}

    public void print(){
        for(int i=0; i<solution.size();i++){
            solution.get(i).printState();
        }
    }

}
