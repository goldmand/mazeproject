package algorithms.mazeGenerators.search;

public interface ISearchingAlgorithm {
    Solution solve(ISearchable s);
    String getName();

    String getNumberOfNodesEvaluated();

}
