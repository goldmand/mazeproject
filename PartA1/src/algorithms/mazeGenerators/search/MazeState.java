package algorithms.mazeGenerators.search;

import algorithms.mazeGenerators.Position;

public class MazeState extends AState{

    private Position mstate;

    public MazeState() {
        Position p = new Position();
        mstate = p.fromString(this.getState(),p);}

    public MazeState(String state) {
        Position p = new Position();
        mstate = p.fromString(state,p);
    }
    public MazeState(Position p) {
        mstate = p;
    }

    @Override
    public boolean equals(Object obj) {
        MazeState other= (MazeState) obj;
        return other.getMstate().isEqual(this.getMstate());
    }


    public Position getMstate() {
        return mstate;
    }

    public boolean isEqual(MazeState m){return this.mstate.isEqual(m.mstate);} // if positions are identical its the same state
    @Override
    public String toString(){
        return("{"+this.mstate.getRowIndex()+","+this.mstate.getColumnIndex()+"}");
    }
    public void printMazeState(){this.mstate.printPosition();}

    @Override
    public void printState() {
        System.out.println("["+this.mstate.getRowIndex()+","+this.mstate.getColumnIndex()+"]");
    }


}