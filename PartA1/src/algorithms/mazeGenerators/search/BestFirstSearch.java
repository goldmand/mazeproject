package algorithms.mazeGenerators.search;

import java.util.*;
public class BestFirstSearch extends BreadthFirstSearch{

    public BestFirstSearch() {
        this.q = new PriorityQueue<AState>(new Comparator<AState>() {
            @Override
            public int compare(AState s1, AState s2){ //does it sort in uprising order order?
                if (s1.getCost() < s2.getCost())
                    return 1;
                else if (s1.getCost() > s2.getCost())
                    return -1;
                else
                    return 0;
            }
        });
    }


    public String getName(){return "Best First Search";}

}
