package algorithms.mazeGenerators.search;
import java.io.*;
import java.util.*;

public interface ISearchable {
    AState getStartState();
    AState getGoalState();
    ArrayList<AState> getAllSuccessors(AState s);
    ArrayList<AState> getAllPossibleStates(AState s);

}
