package algorithms.mazeGenerators.search;
import algorithms.mazeGenerators.Maze;
import algorithms.mazeGenerators.Position;

import java.io.*;
import java.util.*;

public class SearchableMaze implements ISearchable{

     private Maze myMaze;



     public SearchableMaze(Maze m){this.myMaze=m;}

     public int getNumofRows(){return myMaze.getNumofrows();}
     public int getNumofCols(){return myMaze.getNumofcols();}


    @java.lang.Override
    public AState getStartState() {
         String s = this.myMaze.getStartPosition().toString();
        return new MazeState(s.substring(1,s.length()-1));
    }

    @java.lang.Override
    public AState getGoalState() {

        String s = this.myMaze.getGoalPosition().toString();
        return new MazeState(s.substring(1,s.length()-1));
     }
    public ArrayList<AState> getAllPossibleStates(AState s) {
        ArrayList<AState> p = new ArrayList<AState>();
        MazeState ms = (MazeState)s;
        int row = ms.getMstate().getRowIndex();
        int col = ms.getMstate().getColumnIndex();
        Position pos = new Position();
        pos = ms.getMstate();
        boolean up = row - 1 >= 0;
        boolean down = row + 1 < myMaze.getNumofrows();
        boolean right = col + 1 < myMaze.getNumofcols();
        boolean left = col - 1 >= 0;

        if (col < myMaze.getNumofcols() && row < myMaze.getNumofrows() && (col >= 0) && (row >= 0)){
            //upper neighbour
            if  (up && myMaze.getCellValue(row - 1, col) == 0) {
                AState p2 = new MazeState(String.valueOf(row - 1) + "," + String.valueOf(col));
                p2.setCost(s.getCost() + 1);
                p.add(p2);
            }
            //right neighbor
            if (right && myMaze.getCellValue(row, col + 1) == 0) {
                AState p2 = new MazeState(String.valueOf(row) + "," + String.valueOf(col + 1));
                p2.setCost(s.getCost() + 1);
                p.add(p2);
            }
            //down neighbor
            if (down && myMaze.getCellValue(row + 1, col) == 0) {
                AState p2 = new MazeState(String.valueOf(row + 1) + "," + String.valueOf(col));
                p2.setCost(s.getCost() + 1);
                p.add(p2);
            }
            // left neighbor
            if (left && myMaze.getCellValue(row, col - 1) == 0) {
                AState p2 = new MazeState(String.valueOf(row) + "," + String.valueOf(col - 1));
                p2.setCost(s.getCost() + 1);
                p.add(p2);
            }
            // right up neighbour
            if ((right && up && myMaze.getCellValue(row - 1, col + 1) == 0)
                    && (myMaze.getCellValue(row - 1, col) == 0 || myMaze.getCellValue(row, col + 1) == 0)) {
                AState p2 = new MazeState(String.valueOf(row - 1) + "," + String.valueOf(col + 1));
                p2.setCost(s.getCost() + 2);
                p.add(p2);
            }

            // right down neighbour
            if ((right && down && myMaze.getCellValue(row + 1, col + 1) == 0)
                    && (myMaze.getCellValue(row + 1, col) == 0 || myMaze.getCellValue(row, col + 1) == 0)) {
                AState p2 = new MazeState(String.valueOf(row + 1) + "," + String.valueOf(col + 1));
                p2.setCost(s.getCost() + 2);
                p.add(p2);
            }

            // left down neighbour
            if ((left && down && myMaze.getCellValue(row + 1, col - 1) == 0)
                    && (myMaze.getCellValue(row + 1, col) == 0 || myMaze.getCellValue(row, col - 1) == 0)) {
                AState p2 = new MazeState(String.valueOf(row + 1) + "," + String.valueOf(col - 1));
                p2.setCost(s.getCost() + 2);
                p.add(p2);
            }

            // left up neighbour
            if ((left && up && myMaze.getCellValue(row - 1, col - 1) == 0)
                    && (myMaze.getCellValue(row - 1, col) == 0 || myMaze.getCellValue(row, col - 1) == 0)) {
                AState p2 = new MazeState(String.valueOf(row - 1) + "," + String.valueOf(col - 1));
                p2.setCost(s.getCost() + 2);
                p.add(p2);
            }
        }
        return p;
    }
    @java.lang.Override
    public ArrayList<AState> getAllSuccessors(AState s) {

        Solution sol= new Solution();
        Stack<AState> stacksol = new Stack<AState>();
        stacksol.push(s);
        while(s.getCameFrom()!=null){
            stacksol.add(s.getCameFrom());
            s=s.getCameFrom();
        }
        while(!stacksol.isEmpty())
            sol.insertToSolution(stacksol.pop());
        return sol.getSolutionPath();
    }
}
