package com.company;

import algorithms.mazeGenerators.*;
import algorithms.mazeGenerators.search.BestFirstSearch;
import algorithms.mazeGenerators.search.BreadthFirstSearch;
import algorithms.mazeGenerators.search.SearchableMaze;
import algorithms.mazeGenerators.search.Solution;
import test.RunMazeGenerator;
import test.RunSearchOnMaze;


public class Main {

    public static void main(String[] args) {
	// write your code here
//        Position p1= new Position(0,0);
//        Position p2= new Position(4,4);
//
//        SimpleMazeGenerator g = new SimpleMazeGenerator();
//        Maze m = g.generate(5,5,p1,p2);
//        m.print();

          MyMazeGenerator mg= new MyMazeGenerator();
          Maze m=mg.generate(5,5);
          m.print();
          SearchableMaze sm = new SearchableMaze(m);
          BreadthFirstSearch bfs= new BreadthFirstSearch();
          Solution s = bfs.solve(sm);

        s.print();
        //RunMazeGenerator r = new RunMazeGenerator();

        //RunSearchOnMaze()


    }
}
